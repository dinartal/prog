#include <stdio.h>
#include <string.h>

int main(int argc, char **argv)
{
    if (argc != 3) {
        printf("Usage: prog file text\n");
        return 1;
    }

    unsigned int Counter = 0; //founded string count
    char xorKey[] = "0123456789ABCDEF0123456789ABCDEF";
    size_t xorKeyLen = 32;
    size_t SearchLen; //searching string lenght
    SearchLen = strlen((const char *)(argv[2]));
    char SearchCode[SearchLen][xorKeyLen]; //XORed array for search
    FILE *InFileP;
    char ch;
    size_t i, j;

    for (i = 0; i < SearchLen; i++) {
        for (j = 0; j < xorKeyLen; j++) {
            SearchCode[i][j] = (argv[2][i]) ^ (xorKey[j]);
        }
    }

    InFileP = fopen((const char *)(argv[1]), "rb");
    if (InFileP == NULL) {
        printf("Error opening file.\n");
        return 1;
    }

    i = 0; //reset i for count character matches

    while (1) {
        for (j = 0; j < xorKeyLen; j++) {
            if (feof(InFileP)) {
                printf("I founded \"%s\" in \"%s\" %d times.\n", argv[2], argv[1], Counter);
                return 0;
            } else {
                ch = getc(InFileP);
            }

            if (ch == SearchCode[i][j]) {
                i++;
                if (i == SearchLen) {
                    Counter++;
                    i = 0;
                }
            } else if (ch == SearchCode[0][j]) {
                i = 1;
            } else {
                i = 0;
            }
        }
    }
    return 0;
}
