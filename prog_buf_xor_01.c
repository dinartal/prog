//ver with buffering and with runtime xor

#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>

size_t SearchLen; //searching string lenght
char** SearchCode = NULL; //[SearchLen][xorKeyLen]; //XORed array for search
char* Buf;
char* Searching;
void freeMem(void); //function for free allocated memory

int main(int argc, char **argv)
{
	clock_t t;
	t = clock();
	
    if (argc != 3) {
        printf("Usage: prog file text\n");
        return 1;
    }

    unsigned int Counter = 0; //founded string count
    char xorKey[] = "0123456789ABCDEF0123456789ABCDEF";
    size_t xorKeyLen = 32;
    
    
    
    FILE *InFileP;
    char ch;
    size_t i, j, bi;
	size_t BufLen;
	
	size_t Buffsize = 3200;
	SearchLen = strlen((const char *)(argv[2]));
	SearchCode = (char**) malloc(SearchLen*sizeof(char*));
	for (i=0;i<SearchLen; i++)
	{
		SearchCode[i]= (char*) malloc(xorKeyLen*sizeof(char));
	}
	Buf = (char*) malloc(Buffsize);
	Searching = (char*) malloc(SearchLen);
	memcpy(Searching,argv[2],SearchLen);
    for (i = 0; i < SearchLen; i++) {
        for (j = 0; j < xorKeyLen; j++) {
            SearchCode[i][j] = (Searching[i]) ^ (xorKey[j]);
        }
    }

    InFileP = fopen((const char *)(argv[1]), "rb");
    if (InFileP == NULL) {
        printf("Error opening file.\n");
		freeMem();
        return 1;
    }

    i = 0; //reset i for count character matches

	
	
    while (1) {
		BufLen = fread(Buf, 1, Buffsize, InFileP);
		if (BufLen)
		{
			j=0;
			for (bi=0; bi<BufLen; bi++)
			{
				if(Buf[bi]==((Searching[i]) ^ (xorKey[j]))){
					i++;
					if (i == SearchLen) {
						Counter++;
						i = 0;
					}
				} else if (Buf[bi] == ((Searching[0]) ^ (xorKey[j]))) {
					i = 1;
				} else {
					i = 0;
				}
				j++;
				if(j==xorKeyLen) j=0;
			}
			
		}
		else
		{
			printf("I founded \"%s\" in \"%s\" %d times.\n", argv[2], argv[1], Counter);
			t = clock() - t;
			printf ("It took me %f seconds.\n",((double)t)/CLOCKS_PER_SEC);
			freeMem();
			return 0;
		}
        
    }
   
	
	freeMem();
    return 0;
}

void freeMem(void)
{
	size_t i;
	for (i=0;i<SearchLen; i++)
	{
		free(SearchCode[i]);
	}
	free(SearchCode);
	free(Buf);
}
